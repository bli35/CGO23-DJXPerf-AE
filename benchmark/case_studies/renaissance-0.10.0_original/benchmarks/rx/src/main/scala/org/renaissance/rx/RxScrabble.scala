package org.renaissance.rx

import org.renaissance.BenchmarkResult
import org.renaissance.Config
import org.renaissance.HashingResult
import org.renaissance.License
import org.renaissance.RenaissanceBenchmark
import org.renaissance.Benchmark._

import java.util.Map.Entry
import java.util.List

@Name("rx-scrabble")
@Group("rx")
@Summary("Solves the Scrabble puzzle using the Rx streams.")
@Licenses(Array(License.GPL2))
@Repetitions(80)
class RxScrabble extends RenaissanceBenchmark {

  class RxScrabbleResult(actualResult: List[Entry[Integer, List[String]]], expectedHash: String)
    extends BenchmarkResult {

    override def validate(): Unit = {
      val actualWords = RxScrabbleImplementation.prepareForValidation(actualResult)

      val hasher = new HashingResult(expectedHash, actualWords)
      hasher.validate()
    }
  }

  // TODO: Consolidate benchmark parameters across the suite.
  //  See: https://github.com/renaissance-benchmarks/renaissance/issues/27

  var shakespearePath: String = "/shakespeare.txt"

  var scrabblePath: String = "/scrabble.txt"

  var bench: RxScrabbleImplementation = null

  override def setUpBeforeAll(c: Config): Unit = {
    if (c.functionalTest) {
      shakespearePath = "/shakespeare-truncated.txt"
    }
    bench = new RxScrabbleImplementation(scrabblePath, shakespearePath)
  }

  override def runIteration(c: Config): BenchmarkResult = {
    val result = bench.runScrabble()
    return new RxScrabbleResult(
      result,
      if (c.functionalTest) "a7b6836b27dbdf0f" else "7527985ec20d9aab"
    )
  }
}
