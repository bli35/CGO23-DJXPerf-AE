#!/bin/bash

#JAVA_PATH=/home/psu/Tools/java/jdk10/install/bin
#PROFILER_HOME=/home/psu/Tools/java/java-sampling-profiler

#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/psu/Tools/java/jdk10/install/jvm/openjdk-10-internal/lib/server

#LD_PRELOAD=/home/bli11/data_centric/data_centric_java_profiler/build/libpreload.so java -javaagent:/home/bli11/data_centric/modified_allocation_callback/target/java-allocation-instrumenter-3.1.0.jar -agentpath:/home/bli11/data_centric/data_centric_java_profiler/build/libagent.so=MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000 -jar target/RankLib-2.11-SNAPSHOT.jar -train MQ2008/Fold1/train.txt -ranker 4 -kcv 5 -kcvmd models/ -kcvmn ca -metric2t NDCG@10 -metric2T ERR@10
LD_PRELOAD=/home/bli11/data_centric/data_centric_java_profiler/build/libpreload.so java -javaagent:/home/bli11/data_centric/modified_allocation_callback/target/java-allocation-instrumenter-3.1.0.jar -agentpath:/home/bli11/data_centric/data_centric_java_profiler/build/libagent.so=MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000000 -jar bin/RankLib.jar -train MQ2008/Fold1/train.txt
