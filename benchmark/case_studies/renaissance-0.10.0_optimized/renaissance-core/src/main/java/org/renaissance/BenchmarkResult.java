package org.renaissance;

public interface BenchmarkResult {
  public void validate() throws ValidationException;
}
