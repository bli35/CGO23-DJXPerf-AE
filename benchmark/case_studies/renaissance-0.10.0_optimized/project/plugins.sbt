addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "1.6.0-RC2")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.9")
addSbtPlugin("pl.project13.scala" % "sbt-jmh" % "0.3.6")
