================================
SPECjvm2008 Peak
================================
n/a n/a
Oracle Corporation OpenJDK 64-Bit Server VM
Tested by: n/a Test date: Wed Oct 03 21:55:21 EDT 2018
Run is valid, but not compliant

================================
scimark.large                 38.44                                   
Noncompliant composite result: 38.44 ops/m
================================


Submitter                     n/a                                     
Submitter URL                 n/a                                     
SPEC license                  n/a                                     
Tester                        psu                                     
Location                      n/a                                     
Test date:                    Wed Oct 03 21:55:21 EDT 2018            
OS name                       Linux                                   
OS available                  n/a                                     
OS address bits               n/a                                     
OS tuning                                                             
Filesystem                    n/a                                     
Other s/w name                                                        
Other s/w tuning                                                      
Other s/w available                                                   
Vendor                        Oracle Corporation                      
Vendor URL                    http://java.oracle.com/                 
JVM name                      OpenJDK 64-Bit Server VM                
JVM version                   10-internal+0-adhoc.psu.jdk10 mixed mode
JVM available                 n/a                                     
Java Specification            10                                      
JVM address bits              64                                      
JVM initial heap memory       n/a                                     
JVM maximum heap memory       n/a                                     
JVM command line              n/a                                     
JVM command line startup                                              
JVM launcher startup          default                                 
Additional JVM tuning                                                 
JVM class path                SPECjvm2008.jar                         
JVM boot class path           n/a                                     
HW vendor                     n/a                                     
HW vendor's URL               n/a                                     
HW model                      n/a                                     
HW available                  n/a                                     
CPU vendor                    n/a                                     
CPU vendor's URL              n/a                                     
CPU name                      n/a                                     
CPU frequency                 n/a                                     
# of logical cpus             n/a                                     
# of chips                    n/a                                     
# of cores                    n/a                                     
Cores per chip                n/a                                     
Threads per core              n/a                                     
Threading enabled             n/a                                     
HW address bits               n/a                                     
Primary cache                 n/a                                     
Secondary cache               n/a                                     
Other cache                   n/a                                     
Memory size                   n/a                                     
Memory details                n/a                                     
Other HW details              n/a                                     

Property specjvm.run.type must be 2 for publication.
Property specjvm.fixed.operations not allowed in publication run.
Not a compliant sequence of benchmarks for publication.
Property specjvm.run.initial.check must be true for publication.
Checksum test failed for jar files (Checksum test failed on SPECjvm2008.jar). Kit may not be changed or rebuild.

specjvm.benchmark.warmup.time=0
specjvm.benchmark.threads=1

Details of Runs
---------------

scimark.fft.large             iteration 1    null           4683           3.00           38.44          

SPECjvm2008 Version: [SPECjvm2008 1.01 (20090519)]
Copyright (C) 2008-2018 SPEC. All rights reserved
