#!/bin/bash

JAVA_PATH=/home/psu/Tools/java/jdk-11/bin
JAVA_PROFILER_HOME=/home/psu/Tools/java/java-sampling-profiler

export LD_PRELOAD=$JAVA_PROFILER_HOME/build/libpreload.so

$JAVA_PATH/java -agentpath:$JAVA_PROFILER_HOME/build/libagent.so=LoadSpy::MEM_UOPS_RETIRED:ALL_LOADS:precise=3@100000 -XX:+UnlockDiagnosticVMOptions -XX:+DebugNonSafepoints -XX:+PrintAssembly -XX:+PrintInlining -XX:PrintAssemblyOptions=hsdis-print-bytes -XX:+LogCompilation -jar SPECjvm2008.jar -wt 0 -ops 10 -i 3 -bt 4 -ict scimark.fft.large

native="$JAVA_PATH/java -jar SPECjvm2008.jar -wt 0 -ops 10 -i 3 -bt 4 -ict scimark.fft.large"
profile="$JAVA_PATH/java -agentpath:$JAVA_PROFILER_HOME/build/libagent.so=LoadSpy::MEM_UOPS_RETIRED:ALL_LOADS:precise=3@500000 -jar SPECjvm2008.jar -wt 0 -ops 10 -i 3 -bt 4 -ict scimark.fft.large"

#taskset -c 0-17 $profile
#{ time taskset -c 0-17 $native > out 2>&1 ; } 2>> runtime
#{ time taskset -c 0-17 $profile > out 2>&1 ; } 2>> runtime
