#!/bin/bash

JAVA_PATH=/home/psu/tools/java/jdk10/install/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/psu/tools/java/jdk10/install/jvm/openjdk-10-internal/lib/server
JAVA_PROFILER_HOME=/home/psu/tools/java/java-sampling-profiler

LD_PRELOAD=$JAVA_PROFILER_HOME/build/libpreload.so $JAVA_PATH/java -agentpath:$JAVA_PROFILER_HOME/build/libagent.so=RedSpy::MEM_UOPS_RETIRED:ALL_STORES:precise=3@10000 -XX:-Inline -jar SPECjvm2008.jar -wt 0 -ops 10 -i 3 -bt 1 -ict scimark.sor.large 

