#setup
. /home/bli11/spack/spack/share/spack/setup-env.sh
spack load py-bintrees@2.0.7
spack load py-protobuf@3.12.2

TIMEFORMAT=%R

#All Case Studies
#################

#################
#Dacapo Lusearch
#original run with profier:
rm -rf ./lusearch_results
rm ./metric.txt
touch metric.txt
mkdir lusearch_results
echo "Application: Dacapo Lusearch " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt 

LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=DataCentric::MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000000 -jar $JXPerf_HOME/benchmark/case_studies/dacapo/dacapo-ori.jar -s large lusearch 2&>1 ; } 2>> metric.txt
mv *.run ./lusearch_results
cd ./lusearch_results
../offline.sh
cd ..

#native original run:
echo -n "Original Execution time without DJXPerf: " >> metric.txt 
{ time java -jar $JXPerf_HOME/benchmark/case_studies/dacapo/dacapo-ori.jar -s large lusearch > /dev/null 2>&1 ; } 2>> metric.txt 

#native optimized run:
echo -n "Optimized Execution time without DJXPerf: " >> metric.txt 
{ time java -jar $JXPerf_HOME/benchmark/case_studies/dacapo/dacapo-op.jar -s large lusearch > /dev/null 2>&1 ; } 2>> metric.txt

#################
#FindBug
#original run with profiler
rm -rf ./findbug_results
mkdir findbug_results
echo "=============" >> metric.txt
echo "Application: FindBug " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt 
LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=DataCentric::MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000000 -jar $JXPerf_HOME/benchmark/case_studies/findbugs-3.0.1_original/lib/findbugs-ori.jar -textui -effort:max $JXPerf_HOME/benchmark/case_studies/findbugs-3.0.1_original/jfreechart-1.0.19-demo.jar >/dev/null 2>&1 ; } 2>> metric.txt
mv *.run ./findbug_results
cd ./findbug_results
../offline.sh
cd ..

#native original run
echo -n "Original Execution time without DJXPerf: " >> metric.txt 
{ time java -jar $JXPerf_HOME/benchmark/case_studies/findbugs-3.0.1_original/lib/findbugs-ori.jar -textui -effort:max $JXPerf_HOME/benchmark/case_studies/findbugs-3.0.1_original/jfreechart-1.0.19-demo.jar 2>&1 ; } 2>> metric.txt  

#native optimized run:
echo -n "Optimized Execution time without DJXPerf: " >> metric.txt 
{ time java -jar $JXPerf_HOME/benchmark/case_studies/findbugs-3.0.1_optimized/lib/findbugs-op.jar -textui -effort:max $JXPerf_HOME/benchmark/case_studies/findbugs-3.0.1_optimized/jfreechart-1.0.19-demo.jar 2>&1 ; } 2>> metric.txt 

#################
#Ranklib
#original run with profiler:
rm -rf ./ranklib_results
mkdir ranklib_results
echo "=============" >> metric.txt
echo "Application: Ranklib " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt
LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=DataCentric::MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000000 -jar $JXPerf_HOME/benchmark/case_studies/ranklib_original/bin/RankLib-ori.jar -train $JXPerf_HOME/benchmark/case_studies/ranklib_original/MQ2008/Fold1/train.txt >/dev/null 2>&1 ; } 2>> metric.txt
mv *.run ./ranklib_results
cd ./ranklib_results
../offline.sh
cd ..

#native original run:
echo -n "Original Execution time without DJXPerf: " >> metric.txt 
{ time java -jar $JXPerf_HOME/benchmark/case_studies/ranklib_original/bin/RankLib-ori.jar -train $JXPerf_HOME/benchmark/case_studies/ranklib_original/MQ2008/Fold1/train.txt >/dev/null 2>&1 ; } 2>> metric.txt

#native optimized run:
echo -n "Optimized Execution time without DJXPerf: " >> metric.txt 
{ time java -jar $JXPerf_HOME/benchmark/case_studies/ranklib_optimized/bin/RankLib-op.jar -train $JXPerf_HOME/benchmark/case_studies/ranklib_optimized/MQ2008/Fold1/train.txt >/dev/null 2>&1 ; } 2>> metric.txt
#################
#cache2k
#original run with profiler:
rm -rf ./cache2k_results
mkdir cache2k_results
echo "=============" >> metric.txt
echo "Application: Cache2k " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt
LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=DataCentric::MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@100000 -jar $JXPerf_HOME/benchmark/case_studies/cache2k_original/cache2k-benchmark/jmh-suite/target/benchmarks.jar -f 0 -wi 10 -i 10 org.cache2k.benchmark.jmh.attic.CombinedReadWriteBenchmark.readOnly >/dev/null 2>&1 ; } 2>> metric.txt
mv *.run ./cache2k_results
cd ./cache2k_results
../offline.sh
cd ..

#native original run:
echo -n "Original Execution time without DJXPerf: " >> metric.txt 
{ time java -jar $JXPerf_HOME/benchmark/case_studies/cache2k_original/cache2k-benchmark/jmh-suite/target/benchmarks.jar -f 0 -wi 2 -i 2 org.cache2k.benchmark.jmh.attic.CombinedReadWriteBenchmark.readOnly >/dev/null 2>&1 ; } 2>> metric.txt

#native optimized run:
echo -n "Optimized Execution time without DJXPerf: " >> metric.txt 
{ time java -jar $JXPerf_HOME/benchmark/case_studies/cache2k_optimized/cache2k-benchmark/jmh-suite/target/benchmarks.jar -f 0 -wi 2 -i 2 org.cache2k.benchmark.jmh.attic.CombinedReadWriteBenchmark.readOnly >/dev/null 2>&1 ; } 2>> metric.txt
#################
#JGFMonteCarloBench
#original run with profiler
rm -rf ./JGFMonteCarloBench_results
mkdir JGFMonteCarloBench_results
echo "=============" >> metric.txt
echo "Application: JGFMonteCarloBench " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt
cd $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_original
LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=DataCentric::MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000 -cp $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_original JGFMonteCarloBenchSizeB >/dev/null 2>&1 ; } 2>> ../metric.txt
mv ./*.run $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_results
cd $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_results
../offline.sh

#native original run
echo -n "Original Execution time without DJXPerf: " >> ../metric.txt 
cd $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_original
{ time java -cp $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_original JGFMonteCarloBenchSizeB >/dev/null 2>&1 ; } 2>> ../metric.txt

#native optimized run
echo -n "Optimized Execution time without DJXPerf: " >> ../metric.txt 
cd $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_optimized
{ time java -cp $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_optimized JGFMonteCarloBenchSizeB >/dev/null 2>&1 ; } 2>> ../metric.txt
#################
#JGFMolDynBench
#original run with profiler
cd $JXPerf_HOME/benchmark/case_studies
rm -rf ./JGFMolDynBench_results
mkdir JGFMolDynBench_results
echo "=============" >> metric.txt
echo "Application: JGFMolDynBench " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt
cd $JXPerf_HOME/benchmark/case_studies/JGFMolDynBench_original
LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=DataCentric::MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@10000 -cp $JXPerf_HOME/benchmark/case_studies/JGFMolDynBench_original JGFMolDynBenchSizeB >/dev/null 2>&1 ; } 2>> ../metric.txt
mv ./*.run $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_results
cd $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_results
../offline.sh

#native original run
echo -n "Original Execution time without DJXPerf: " >> ../metric.txt
cd $JXPerf_HOME/benchmark/case_studies/JGFMolDynBench_original
{ time java -cp $JXPerf_HOME/benchmark/case_studies/JGFMolDynBench_original JGFMolDynBenchSizeB >/dev/null 2>&1 ; } 2>> ../metric.txt

#native optimized run
echo -n "Optimized Execution time without DJXPerf: " >> ../metric.txt
cd $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_optimized
{ time java -cp $JXPerf_HOME/benchmark/case_studies/JGFMonteCarloBench_optimized JGFMolDynBenchSizeB >/dev/null 2>&1 ; } 2>> ../metric.txt
#################
#Eclipse-collection
#original run with profiler
cd $JXPerf_HOME/benchmark/case_studies
rm -rf ./Eclipse_results
mkdir Eclipse_results
echo "=============" >> metric.txt
echo "Application: Eclipse " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt
LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=NUMA::MEM_UOPS_RETIRED:ALL_LOADS:precise=2@10000 -jar $JXPerf_HOME/benchmark/case_studies/eclipse-collections_original/jmh-tests/benchmarks.jar com.carrotsearch.hppc.benchmarks.B001_ModXor.modOp -wi 1 -i 1 -f 0 >/dev/null 2>&1 ; } 2>> metric.txt
mv *.run ./Eclipse_results
cd ./Eclipse_results
../offline.sh
cd ..

#native original run
echo -n "Original Execution time without DJXPerf: " >> metric.txt
{ time java -jar $JXPerf_HOME/benchmark/case_studies/eclipse-collections_original/jmh-tests/benchmarks.jar com.carrotsearch.hppc.benchmarks.B001_ModXor.modOp -wi 1 -i 1 -f 0 >/dev/null 2>&1 ; } 2>> metric.txt

#native optimized run
echo -n "Optimized Execution time without DJXPerf: " >> metric.txt
{ time java -jar $JXPerf_HOME/benchmark/case_studies/eclipse-collections_optimized/jmh-tests/benchmarks.jar com.carrotsearch.hppc.benchmarks.B001_ModXor.modOp -wi 1 -i 1 -f 0 >/dev/null 2>&1 ; } 2>> metric.txt
#################
#NPB SP
#original run with profiler
rm -rf ./NPB_results
mkdir NPB_results
echo "=============" >> metric.txt
echo "Application: NPB " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt
export CLASSPATH=$JXPerf_HOME/benchmark/case_studies/NPB3.0-JAV_original;
LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=NUMA::MEM_UOPS_RETIRED:ALL_LOADS:precise=2@10000 NPB3_0_JAV.SP -np8 CLASS=S >/dev/null 2>&1 ; } 2>> metric.txt
mv *.run ./NPB_results
cd ./NPB_results
../offline.sh
cd ..

#native original run
echo -n "Original Execution time without DJXPerf: " >> metric.txt
export CLASSPATH=$JXPerf_HOME/benchmark/case_studies/NPB3.0-JAV_original;
{ time java NPB3_0_JAV.SP -np8 CLASS=S >/dev/null 2>&1 ; } 2>> metric.txt

#native optimized run
echo -n "Optimized Execution time without DJXPerf: " >> metric.txt
export CLASSPATH=$JXPerf_HOME/benchmark/case_studies/NPB3.0-JAV_optimized;
{ time java NPB3_0_JAV.SP -np8 CLASS=S >/dev/null 2>&1 ; } 2>> metric.txt
#################
#Apache Druid
#original run with profiler
rm -rf ./Druid_results
mkdir Druid_results
echo "=============" >> metric.txt
echo "Application: Druid " >> metric.txt 
echo -n "Execution time with DJXPerf: " >> metric.txt
LD_PRELOAD=$JXPerf_HOME/build/preload/libpreload.so
{ time java -javaagent:$JXPerf_HOME/thirdparty/allocation-instrumenter/target/java-allocation-instrumenter-HEAD-SNAPSHOT.jar -agentpath:$JXPerf_HOME/build/libagent.so=NUMA::MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000000 -jar $JXPerf_HOME/benchmark/case_studies/apache_druid_original/benchmarks/target/benchmarks.jar org.apache.druid.benchmark.DataSketchesHllBenchmark.init -wi 0 -i 1 -f 0 >/dev/null 2>&1 ; } 2>> metric.txt
mv *.run ./Druid_results
cd ./Druid_results
../offline.sh
cd ..

#native original run
echo -n "Original Execution time without DJXPerf: " >> metric.txt
{ time java -jar $JXPerf_HOME/benchmark/case_studies/apache_druid_original/benchmarks/target/benchmarks.jar org.apache.druid.benchmark.DataSketchesHllBenchmark.init -wi 0 -i 1 -f 0 >/dev/null 2>&1 ; } 2>> metric.txt

#native optimized run
echo -n "Optimized Execution time without DJXPerf: " >> metric.txt
{ time java -jar $JXPerf_HOME/benchmark/case_studies/apache_druid_optimized/benchmarks/target/benchmarks.jar org.apache.druid.benchmark.DataSketchesHllBenchmark.init -wi 0 -i 1 -f 0 >/dev/null 2>&1 ; } 2>> metric.txt