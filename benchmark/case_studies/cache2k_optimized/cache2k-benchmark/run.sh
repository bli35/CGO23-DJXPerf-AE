#!/bin/bash

#LD_PRELOAD=/home/bli11/data_centric/data_centric_java_profiler/build/libpreload.so java -javaagent:/home/bli11/data_centric/modified_allocation_callback/target/java-allocation-instrumenter-3.1.0.jar -agentpath:/home/bli11/data_centric/data_centric_java_profiler/build/libagent.so=MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000 -jar jmh-suite/target/benchmarks.jar -f 0 -wi 3 -i 3 -r 1 -w 1 org.cache2k.benchmark.jmh.suite.noEviction.asymmetrical.CombinedReadWriteBenchmark.readOnly

#LD_PRELOAD=/home/bli11/data_centric/data_centric_java_profiler/build/libpreload.so java -javaagent:/home/bli11/data_centric/modified_allocation_callback/target/java-allocation-instrumenter-3.1.0.jar -agentpath:/home/bli11/data_centric/data_centric_java_profiler/build/libagent.so=MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000 -jar jmh-suite/target/benchmarks.jar -f 0 -wi 10 -i 10 org.cache2k.benchmark.jmh.suite.noEviction.asymmetrical.CombinedReadWriteBenchmark.readOnly
LD_PRELOAD=/home/bli11/data_centric/data_centric_java_profiler/build/libpreload.so java -javaagent:/home/bli11/data_centric/modified_allocation_callback/target/java-allocation-instrumenter-3.1.0.jar -agentpath:/home/bli11/data_centric/data_centric_java_profiler/build/libagent.so=MEM_LOAD_UOPS_RETIRED:L1_MISS:precise=2@1000 -jar jmh-suite/target/benchmarks.jar -f 0 -wi 10 -i 10 org.cache2k.benchmark.jmh.suite.noEviction.symmetrical.IntReadOnlyBenchmark





#JAVA_PATH=/home/psu/Tools/java/jdk10/install/bin
#PROFILER_HOME=/home/psu/Tools/java/java-sampling-profiler

#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/psu/Tools/java/jdk10/install/jvm/openjdk-10-internal/lib/server

#LD_PRELOAD=$PROFILER_HOME/build/libpreload.so $JAVA_PATH/java -agentpath:$PROFILER_HOME/build/libagent.so=LoadSpy::MEM_UOPS_RETIRED:ALL_LOADS:precise=3@100000 -jar jmh-suite/target/benchmarks.jar -f 0 -wi 3 -i 3 -r 1 -w 1 org.cache2k.benchmark.jmh.suite.noEviction.asymmetrical.CombinedReadWriteBenchmark.readOnly

#LD_PRELOAD=$PROFILER_HOME/build/libpreload.so $JAVA_PATH/java -agentpath:$PROFILER_HOME/build/libagent.so=LoadSpy::MEM_UOPS_RETIRED:ALL_LOADS:precise=3@100000 -XX:+UnlockDiagnosticVMOptions -XX:+DebugNonSafepoints -XX:+PrintAssembly -XX:+PrintInlining -XX:PrintAssemblyOptions=hsdis-print-bytes -XX:+LogCompilation -jar jmh-suite/target/benchmarks.jar -f 0 -wi 3 -i 3 -r 1 -w 1 org.cache2k.benchmark.jmh.suite.noEviction.asymmetrical.CombinedReadWriteBenchmark.readOnly
